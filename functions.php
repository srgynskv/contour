<?php
/**
 * Contour functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Contour
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Theme info.
 */
require get_template_directory() . '/includes/theme-info.php';

/**
 * Theme setup.
 */
require get_template_directory() . '/includes/setup.php';

/**
 * Widget areas.
 */
require get_template_directory() . '/includes/widgets.php';

/**
 * Page head.
 */
require get_template_directory() . '/includes/head.php';

/**
 * Styles and scripts.
 */
require get_template_directory() . '/includes/scripts.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';
