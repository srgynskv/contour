# [Contour](https://contour.noskov.biz/)

[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](http://gruntjs.com/)

Contour is a [WordPress](https://wordpress.org/) starter theme based on [Underscores](http://underscores.me/), [Foundation for Sites](http://foundation.zurb.com/sites.html), [Motion UI](https://github.com/zurb/motion-ui), and [Font Awesome](http://fontawesome.io/). Built with [Grunt](http://gruntjs.com/) and [Bower](http://bower.io/).
