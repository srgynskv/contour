<?php
/**
 * Theme info.
 *
 * @package Contour
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

global $contour;

$theme_info = wp_get_theme( 'contour' );

$contour['theme'] = array(
	'name'        => $theme_info->get( 'Name' ),
	'alias'       => strtolower( $theme_info->get( 'Name' ) ),
	'theme_uri'   => $theme_info->get( 'ThemeURI' ),
	'description' => $theme_info->get( 'Description' ),
	'author'      => $theme_info->get( 'Author' ),
	'author_uri'  => $theme_info->get( 'AuthorURI' ),
	'version'     => $theme_info->get( 'Version' ),
	'template'    => $theme_info->get( 'Template' ),
	'status'      => $theme_info->get( 'Status' ),
	'tags'        => $theme_info->get( 'Tags' ),
	'text_domain' => $theme_info->get( 'TextDomain' ),
	'domain_path' => $theme_info->get( 'DomainPath' ),
);
