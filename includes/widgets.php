<?php
/**
 * Contour widgets.
 *
 * @package Contour
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Registers widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function contour_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'contour' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'contour_widgets_init' );
