<?php
/**
 * Contour styles and scripts.
 *
 * @package Contour
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'contour_deregister_scripts' ) ) :
/**
 * Removes registered scripts.
 *
 * @return void
 */
function contour_deregister_scripts() {
	// jQuery
	wp_deregister_script( 'jquery' );
}
endif;
add_action( 'wp_enqueue_scripts', 'contour_deregister_scripts' );

if ( ! function_exists( 'contour_scripts' ) ) :
/**
 * Enqueues styles and scripts.
 *
 * @return void
 */
function contour_scripts() {
	global $contour;

	// Styles
	wp_enqueue_style( 'contour-style', get_template_directory_uri() . '/style.css', array(), $contour['theme']['version'], 'all' );

	// Scripts
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), $contour['theme']['version'], true );
	wp_enqueue_script( 'contour-script', get_template_directory_uri() . '/assets/js/contour.min.js', array( 'jquery' ), $contour['theme']['version'], true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
endif;
add_action( 'wp_enqueue_scripts', 'contour_scripts' );
