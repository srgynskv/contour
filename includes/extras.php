<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Contour
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'dump' ) ) :
/**
 * Dumps the information of the variable.
 *
 * @param  string  $var   The variable to dump.
 * @param  boolean $print To use print_r() or var_dump().
 * @param  boolean $die   To use wp_die() or not.
 * @return void
 */
function dump( $var = '', $print = true, $die = false ) {
	$dump = $print ? 'print_r' : 'var_dump';

	echo '<pre>';
	$dump( $var );
	echo '</pre>';

	$die && wp_die();
}
endif;

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function contour_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'contour_body_classes' );
