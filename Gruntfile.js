// Gruntfile
module.exports = function( grunt ) {
  'use strict';

  // Requirements
  require( 'time-grunt' )( grunt );

  require( 'jit-grunt' )( grunt, {
    makepot: 'grunt-wp-i18n',
  });

  // Config
  grunt.initConfig( {
    // info
    pkg: grunt.file.readJSON( 'package.json' ),

    // clean
    clean: {
      options: {
        force: true,
      },
      dist: {
        src: [
          '<%= pkg.dist %>/',
        ],
      },
    },

    // copy
    copy: {
      jquery: {
        expand: true,
        cwd: 'bower_components/jquery/dist/',
        src: 'jquery.min.js',
        dest: '<%= pkg.assets.js %>',
        flatten: true,
      },
      foundation: {
        expand: true,
        cwd: 'bower_components/foundation-sites/scss/',
        src: [
          'foundation.scss',
          'settings/_settings.scss',
        ],
        dest: '<%= pkg.assets.scss %>',
        flatten: true,
      },
      font_awesome: {
        expand: true,
        cwd: 'bower_components/font-awesome/fonts/',
        src: '**',
        dest: '<%= pkg.assets.fonts %>',
        flatten: true,
      },
      dist: {
        expand: true,
        cwd: '',
        src: [
          '**',
          '!.*',
          '!*.{err,json,log,map,md,xml,zip}',
          '!.*/**',
          '!Gruntfile.js',
          '!LICENSE',
          '!<%= pkg.assets.js %>/**/*.{js,map}',
          '<%= pkg.assets.js %>/**/*.min.js',
          '!<%= pkg.assets.sass %>/**',
          '!<%= pkg.assets.scss %>/**',
          '!bower_components/**',
          '!node_modules/**',
          '!<%= pkg.dist %>/**',
        ],
        dest: '<%= pkg.dist %>',
        flatten: false,
      },
    },

    // sass
    sass: {
      options: {
        includePaths: [
          'bower_components/foundation-sites/scss/',
          'bower_components/motion-ui/src/',
          'bower_components/font-awesome/scss/',
        ],
        outputStyle: 'compressed',
        sourceMap: false,
      },
      theme: {
        files: {
          'style.css': '<%= pkg.assets.scss %>/style.scss',
        },
      },
    },

    // postcss
    postcss: {
      options: {
        processors: [
          require( 'pixrem' )(),
          require( 'autoprefixer' )( {
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
          } ),
          require( 'cssnano' )(),
        ],
        map: false,
      },
      theme: {
        src: 'style.css',
      },
    },

    // cssmin
    cssmin: {},

    // jshint
    jshint: {
      options: {
        browser: true,
        jquery: true,
        globals: {
          jQuery: true,
          Backbone: true,
          wp: true,
          _: true,
        },
      },
      files: [
        'Gruntfile.js',
        '<%= pkg.assets.js %>/**/*.js',
        '!<%= pkg.assets.js %>/<%= pkg.name %>.js',
        '!<%= pkg.assets.js %>/**/*.min.js',
      ],
    },

    // concat
    concat: {
      options: {
        // separator: ';',
      },
      theme: {
        src: [
          'bower_components/what-input/what-input.js',

          'bower_components/foundation-sites/js/foundation.core.js',

          'bower_components/foundation-sites/js/foundation.util.box.js',
          'bower_components/foundation-sites/js/foundation.util.keyboard.js',
          'bower_components/foundation-sites/js/foundation.util.mediaQuery.js',
          'bower_components/foundation-sites/js/foundation.util.motion.js',
          'bower_components/foundation-sites/js/foundation.util.nest.js',
          'bower_components/foundation-sites/js/foundation.util.timerAndImageLoader.js',
          'bower_components/foundation-sites/js/foundation.util.touch.js',
          'bower_components/foundation-sites/js/foundation.util.triggers.js',

          'bower_components/foundation-sites/js/foundation.abide.js',
          'bower_components/foundation-sites/js/foundation.accordion.js',
          'bower_components/foundation-sites/js/foundation.accordionMenu.js',
          'bower_components/foundation-sites/js/foundation.drilldown.js',
          'bower_components/foundation-sites/js/foundation.dropdown.js',
          'bower_components/foundation-sites/js/foundation.dropdownMenu.js',
          'bower_components/foundation-sites/js/foundation.equalizer.js',
          'bower_components/foundation-sites/js/foundation.interchange.js',
          'bower_components/foundation-sites/js/foundation.magellan.js',
          'bower_components/foundation-sites/js/foundation.offcanvas.js',
          'bower_components/foundation-sites/js/foundation.orbit.js',
          'bower_components/foundation-sites/js/foundation.responsiveMenu.js',
          'bower_components/foundation-sites/js/foundation.responsiveToggle.js',
          'bower_components/foundation-sites/js/foundation.reveal.js',
          'bower_components/foundation-sites/js/foundation.slider.js',
          'bower_components/foundation-sites/js/foundation.sticky.js',
          'bower_components/foundation-sites/js/foundation.tabs.js',
          'bower_components/foundation-sites/js/foundation.toggler.js',
          'bower_components/foundation-sites/js/foundation.tooltip.js',

          'bower_components/motion-ui/motion-ui.js',

          '<%= pkg.assets.js %>/foundation-init.js',
        ],
        dest: '<%= pkg.assets.js %>/<%= pkg.name %>.js',
      },
    },

    // babel
    babel: {
      customizer: {
        files: {
          '<%= pkg.assets.js %>/customizer.min.js': '<%= pkg.assets.js %>/customizer.js',
        },
      },
      theme: {
        files: {
          '<%= pkg.assets.js %>/<%= pkg.name %>.min.js': '<%= pkg.assets.js %>/<%= pkg.name %>.js',
        },
      },
    },

    // checktextdomain
    checktextdomain: {
      options: {
        text_domain: '<%= pkg.name %>',
        keywords: [
          '__:1,2d',
          '_e:1,2d',
          '_x:1,2c,3d',
          'esc_html__:1,2d',
          'esc_html_e:1,2d',
          'esc_html_x:1,2c,3d',
          'esc_attr__:1,2d',
          'esc_attr_e:1,2d',
          'esc_attr_x:1,2c,3d',
          '_ex:1,2c,3d',
          '_n:1,2,4d',
          '_nx:1,2,4c,5d',
          '_n_noop:1,2,3d',
          '_nx_noop:1,2,3c,4d',
        ],
      },
      theme: {
        src: [
          '**/*.php',
          '!.*/**',
          '!<%= pkg.assets.dir %>/**',
          '!bower_components/**',
          '!node_modules/**',
          '!<%= pkg.dist %>/**',
          '!languages/**',
          '!**/.*',
        ],
        expand: true,
      },
    },

    // makepot
    makepot: {
      theme: {
        options: {
          cwd: '',
          domainPath: '/languages',
          exclude: [
            '<%= pkg.assets.dir %>/*',
            'bower_components/*',
            'node_modules/*',
            '<%= pkg.dist %>/*',
          ],
          mainFile: 'style.css',
          potComments: 'Copyright (c) <%= grunt.template.today( "yyyy" ) %> <%= pkg.author.name %>',
          potHeaders: {
            poedit: true,
            'x-poedit-keywordslist': true,
            'report-msgid-bugs-to': '',
            'language-team': 'LANGUAGE <EMAIL@ADDRESS>',
          },
          type: 'wp-theme',
          updateTimestamp: true,
          updatePoFiles: false,
        },
      },
    },

    // potomo
    potomo: {
      theme: {
        files: [ {
          expand: true,
          cwd: 'languages/',
          src: '*.po',
          dest: 'languages/',
          ext: '.mo',
          nonull: true,
        } ],
      },
    },

    // watch
    watch: {
      options: {
        livereload: true,
      },
      gruntfile: {
        files: [
          'Gruntfile.js',
        ],
        tasks: [
          'build',
        ],
      },
      styles: {
        files: [
          '<%= pkg.assets.scss %>/**/*.scss',
        ],
        tasks: [
          'sass',
          'postcss',
        ],
      },
      scripts: {
        files: [
          '<%= pkg.assets.js %>/**/*.js',
          '!<%= pkg.assets.js %>/**/*.min.js',
        ],
        tasks: [
          'jshint',
          'concat',
          'babel',
        ],
      },
      templates: {
        files: [
          '**/*.php',
          '!{<%= pkg.assets.dir %>,bower_components,languages,node_modules,<%= pkg.dist %>}/**',
        ],
      },
    },
  } );

  // Tasks
  grunt.registerTask( 'install', [
    'copy:foundation',
    'copy:font_awesome',
    'copy:jquery',
    'build',
  ] );

  grunt.registerTask( 'default', [
    'watch',
  ] );

  grunt.registerTask( 'build', [
    'sass',
    'postcss',
    'jshint',
    'concat',
    'babel',
  ] );

  grunt.registerTask( 'lang', [
    'checktextdomain',
    'makepot',
    'potomo',
  ] );

  grunt.registerTask( 'dist', [
    'clean',
    'copy:dist',
  ] );
};
